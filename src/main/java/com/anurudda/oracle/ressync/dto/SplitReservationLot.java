package com.anurudda.oracle.ressync.dto;

import java.util.Set;

public class SplitReservationLot {

    public final Set<SplitPNRDTO> splitPNRDTOS;
    public final ParentPNRDTO parentPNRDTO;
    public final CarrierTuple tuple;

    public SplitReservationLot(CarrierTuple tuple, ParentPNRDTO parentPNRDTO, Set<SplitPNRDTO> splitPNRDTOS) {
        this.tuple = tuple;
        this.parentPNRDTO = parentPNRDTO;
        this.splitPNRDTOS = splitPNRDTOS;
    }
}
