package com.anurudda.oracle.ressync.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper=false)
public class PassengerDTO extends BaseDTO{
    private final String carrierCode;

    public PassengerDTO(String carrierCode){
        this.carrierCode = carrierCode;
    }
    private List<PaxTransactionDTO> paxTransactions = new ArrayList<>();


    public String getPNR() {
        return (String) getValue("PNR");
    }

    public Long getPnrPaxId() {
        return (Long) getValue("PNR_PAX_ID");
    }
    public Long getPaxSequence() {
        return (Long) getValue("PAX_SEQUENCE");
    }

    public String getUniquePaxSeqNo() {
        return getPNR()+"|"+getPaxSequence();
    }
}
