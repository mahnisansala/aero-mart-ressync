package com.anurudda.oracle.ressync.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class ReservationDTO {
    private BasicReservationDTO basic;
    private List<PassengerDTO> passengers;
    private List<AgentTransactionDTO> agentTransactions = new ArrayList<>();
}
