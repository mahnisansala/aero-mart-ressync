package com.anurudda.oracle.ressync.dto;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

@Component
public class TableConfig {

    public static final int COLUMN_NAME = 0;
    public static final int NOT_NULL_INDICATOR = 1;
    public static final int FIELD_TYPE = 2;
    public static final int SKIP_CONVERSION = 3;
    public static final int SKIP_INSERT = 4;
    public static final int PRESERVE_ORIGINAL = 5;
    public static final int DESTINATION_FIELD = 0;
    public static final int SOUURCE_FIELD = 1;
    private Logger logger = LoggerFactory.getLogger(getClass().getName());

    public TableConfig()  {
        try {
            String [] fileNames = new String[]{"t_reservation.csv", "t_pnr_passenger.csv", "t_pax_transaction.csv",
            "t_pax_ext_carrier_transactions.csv","t_agent_transaction.csv"};
            for(String fileName: fileNames){
                processFileForConfigData(fileName);
            }

            fileNames = new String[] {"t_pax_ext_carrier_tnx_to_t_pax_tnx.csv"};
            for(String fileName: fileNames){
                processFileForMapping(fileName);
            }
        }catch (Exception e ){
            logger.error("ERROR",e);
        }
    }

    private void processFileForMapping(String fileName) {
        try {
            File file = new File(getClass().getClassLoader().getResource(fileName).getFile());
            if (file.exists() && file.isFile()) {
                CSVReader reader = new CSVReader(new FileReader(file));
                List<String[]> fields = reader.readAll();
                String tableKey = fileName.substring(0,fileName.length()-4);
                logger.debug("TABLE MAPPING KEY : {}", tableKey);
                if (!tableToTableMapping.containsKey(tableKey)) {
                    tableToTableMapping.put(tableKey, new HashMap<>());
                }
                boolean isFirst = true;
                for (String[] field : fields) {
                    if(isFirst){
                        isFirst = false;
                        continue;
                    }
                    tableToTableMapping.get(tableKey).put(field[DESTINATION_FIELD],field[SOUURCE_FIELD]);
                }
            }
        }catch(Exception e){
            logger.error("ERROR", e);
        }
    }

    private void processFileForConfigData(String fileName) throws IOException, CsvException {
        File file= new File(getClass().getClassLoader().getResource(fileName).getFile());
        if(file.exists() && file.isFile()) {
            CSVReader reader = new CSVReader(new FileReader(file));
            List<String[]> fields = reader.readAll();
            String tableKey = fileName.substring(0, fileName.length() - 4);
            logger.debug("TABLE KEY : {}", tableKey);
            if (!tableMapping.containsKey(tableKey)) {
                tableMapping.put(tableKey, new HashSet<>());
            }
            boolean isFirst = true;
            for (String[] field : fields) {
                if (isFirst) {
                    isFirst = false;
                    continue;
                }

                String skipConversion = null;
                String skipInsert = null;
                String preserveOriginal = null;
                if (field.length >= 4)
                    skipConversion = field[SKIP_CONVERSION];
                if (field.length >= 5)
                    skipInsert = field[SKIP_INSERT];
                if (field.length >= 6)
                    preserveOriginal = field[PRESERVE_ORIGINAL];

                if (field.length >= 3)
                    tableMapping.get(tableKey).add(new GenericDataDTO(field[COLUMN_NAME],
                            field[NOT_NULL_INDICATOR], field[FIELD_TYPE], skipConversion, skipInsert, preserveOriginal));
            }
        }
    }

    private Map<String, Set<GenericDataDTO>> tableMapping = new HashMap<>();
    private Map<String, Map<String, String>> tableToTableMapping = new HashMap<>();

    public Set<GenericDataDTO> getDataListForTable(String tableName){
        return tableMapping.get(tableName);
    }

    public Map<String, String> getTableToTableMapping(String mappingName){
        return tableToTableMapping.get(mappingName);
    }
}
