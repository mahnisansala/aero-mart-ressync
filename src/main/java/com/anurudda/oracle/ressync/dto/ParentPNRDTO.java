package com.anurudda.oracle.ressync.dto;

import lombok.Data;

import java.util.*;

@Data
public class ParentPNRDTO {
    private String pnr;
    private Date splitDate;
    private Set<String> carrierCodes = new HashSet<>();
    public void addCarrierCodes(Set<String> carrierCodes){
        getCarrierCodes().addAll(carrierCodes);
    }
    public void addCarrierCodes(String... carrierCodes){
        getCarrierCodes().addAll(Arrays.asList(carrierCodes));
    }

}
