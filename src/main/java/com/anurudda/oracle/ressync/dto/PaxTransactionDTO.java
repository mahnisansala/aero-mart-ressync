package com.anurudda.oracle.ressync.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper=false)
public class PaxTransactionDTO extends BaseDTO{

    private String carrierCode;

    public PaxTransactionDTO(String carrierCode){
        this.carrierCode = carrierCode;
    }


    public Date getTransactionDate(){
        return (Date) getValue("TNX_DATE");
    }

    public String getLCCTnxId(){
        return (String) getValue("LCC_UNIQUE_TXN_ID");
    }

    public String getPNR(){
        return (String) getValue("PNR");
    }

    public Long getPNRPaxId() {
        return (Long) getValue("PNR_PAX_ID");
    }

    public Long getPaxSeqNo() {
        return (Long) getValue("PAX_SEQUENCE");
    }

    public String getUniquePaxSeqNo() {
        return getPNR()+"|"+getPaxSeqNo();
    }
}
