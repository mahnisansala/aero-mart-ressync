package com.anurudda.oracle.ressync.dto;

public class InvalidDataException extends RuntimeException{
    public InvalidDataException(){
        super();
    }
    public InvalidDataException(String message){
        super(message);
    }
}
