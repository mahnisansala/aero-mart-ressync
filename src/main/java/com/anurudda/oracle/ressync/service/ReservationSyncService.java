package com.anurudda.oracle.ressync.service;

import com.anurudda.oracle.ressync.dto.*;
import com.anurudda.oracle.ressync.repo.ReservationJdbcRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ReservationSyncService {

    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    @Autowired
    private ReservationJdbcRepo reservationJdbcRepo;
    @Autowired
    private TableConfig tblConfig;
    @Autowired
    private ConfigProperties properties;

    public Set<String> syncMissingDummyBookings(String fromDate, String toDate, Set<String> updateFailedPNRs, String... carrierCodes) {
        logger.info("Finding dummy bookings missing for carriers:{} from:{} to:{}", carrierCodes, fromDate, toDate);
        Set<String> carriers = Stream.of(carrierCodes).collect(Collectors.toSet());
        List<CarrierTuple> tuples = getCarrierCombinations(carriers);
        List<ReservationLot> correctionLots = getDummyReservationMissingPNRs(tuples, fromDate, toDate);
        return correctReservationProblemPnrsLots(correctionLots,updateFailedPNRs);
    }

    public Set<String> syncMissingExtTransactions(String fromDate, String toDate, Set<String> updateFailedPNRs, String... carrierCodes) {
        logger.info("Finding dummy bookings missing for carriers:{} from:{} to:{}", carrierCodes, fromDate, toDate);
        Set<String> carriers = Stream.of(carrierCodes).collect(Collectors.toSet());
        List<CarrierTuple> tuples = getCarrierCombinations(carriers);
        List<ReservationLot> correctionLots = getReservationMissingExtTransactions(tuples, fromDate, toDate);
        Set<String> pnrs = correctionLots.stream().flatMap(l -> l.pnrs.stream()).collect(Collectors.toSet());
        logger.debug("================MISSING EXT TRANSACTIONS DETECTION================");
        logger.debug("==== Ext Tnx Missing pnrs count:{}, pnrs:{}", pnrs.size(), pnrs);
        return correctReservationProblemPnrsLots(correctionLots, updateFailedPNRs);
        //FIXME testing code
//        Set<String> tnxMissingPnrs = Arrays.stream("73824062".split(",")).collect(Collectors.toSet());
//        ReservationLot lot = new ReservationLot(new CarrierTuple("G9", "E5"), tnxMissingPnrs, fromDate, toDate);
//        correctReservationPNRsLot(lot);
    }

    public Set<String> syncAgentAndPaxTransactions( String fromDate, String toDate,Set<String> updateFailedPNRs, String... carrierCodes) {
        Set<CarrierTuple> tuples = Stream.of(carrierCodes).map(code -> new CarrierTuple(code, code)).collect(Collectors.toSet());
        Set<String> failedPNRs = new HashSet<>(updateFailedPNRs);
        for (CarrierTuple tuple : tuples) {
            List<String> mismatchingAgents = reservationJdbcRepo.getAgentPaxTransactionMismatchingAgents(fromDate, toDate, tuple);
            //FIXME TESTING REMOVE
//            List<String> mismatchingAgents = Arrays.stream("RBGHBE449".split(",")).collect(Collectors.toList());
            logger.debug("mismatchingAgents size : {}, agents:{}", mismatchingAgents.size(), mismatchingAgents);

            List<AgentTnxPaxTnxDTO> agentTnxPaxTnxMismatches = reservationJdbcRepo.getAgentPaxTransactionMismatches(fromDate, toDate, tuple, mismatchingAgents);
            logger.debug("tnx missing size : {}", agentTnxPaxTnxMismatches.size());

            if (agentTnxPaxTnxMismatches.size() > 0) {
                Set<String> allPNRs = agentTnxPaxTnxMismatches.stream().map(AgentTnxPaxTnxDTO::getPtPNR).collect(Collectors.toSet());
                List<String> paxTnxMissingPnrs = reservationJdbcRepo.getPNRsForMissingPaxTransactions(fromDate, toDate, tuple, mismatchingAgents);
                if (paxTnxMissingPnrs != null && paxTnxMissingPnrs.size() > 0) {
                    allPNRs.addAll(paxTnxMissingPnrs);
                }
                Map<String, ParentPNRDTO> splitConsideredPNRMaps = getSplitPNRsFromAll(fromDate, toDate, allPNRs, tuples);

                Map<String, Set<SplitPNRDTO>> fullMappedPNRs = getAllSplitRecursiveSplits(fromDate, toDate, tuples,
                        splitConsideredPNRMaps);
                logger.debug("compare splits list parent:{}, child:{}", splitConsideredPNRMaps.keySet().size(), fullMappedPNRs.keySet().size());

                Set<String> allSplitRelatedPNRs = fullMappedPNRs.values().stream().flatMap(Collection::stream).map(SplitPNRDTO::getChildPNR).
                        collect(Collectors.toSet());
                allSplitRelatedPNRs.addAll(fullMappedPNRs.keySet());

                logger.debug(" all split related pnrs count: {}, pnrs: {}", allSplitRelatedPNRs.size(), allSplitRelatedPNRs);
                Set<String> splitUpdateFailedPNRs = fixAllSplitRelatedPNRs(fromDate, toDate, splitConsideredPNRMaps, fullMappedPNRs, tuple, tuples, failedPNRs);
                if(splitUpdateFailedPNRs != null && splitUpdateFailedPNRs.size()>0){
                    failedPNRs.addAll(splitUpdateFailedPNRs);
                }
                Set<String> agentTnxMissingFailedPNRs = fixAgentTransactionMissingOnly(fromDate, toDate, tuple, agentTnxPaxTnxMismatches, allSplitRelatedPNRs, failedPNRs);
                if(agentTnxMissingFailedPNRs != null && agentTnxMissingFailedPNRs.size()>0){
                    failedPNRs.addAll(agentTnxMissingFailedPNRs);
                }
                Set<String> nonSplitNonAgtFailedPNRs = fixAllNonSplitNonAgtTnxMissing(fromDate, toDate, tuple, agentTnxPaxTnxMismatches, allSplitRelatedPNRs, failedPNRs);
            }

        }

        return failedPNRs;

    }

    private Set<String> fixAllNonSplitNonAgtTnxMissing(String fromDate, String toDate, CarrierTuple tuple,
                                                       List<AgentTnxPaxTnxDTO> agentTnxPaxTnxMismatches,
                                                       Set<String> splitRelatedPNRs, Set<String> updateFailedPNRs) {
        Set<String> nonSplitNAgentTnxMissingPNRs = agentTnxPaxTnxMismatches.stream().
                filter(item -> !splitRelatedPNRs.contains(item.getPtPNR())
                        && item.getAtPNR() != null).map(AgentTnxPaxTnxDTO::getPtPNR).collect(Collectors.toSet());
        logger.debug("non split & non AGT missing pnrs count:{} pnrs: {}", nonSplitNAgentTnxMissingPNRs.size(),
                nonSplitNAgentTnxMissingPNRs);
        Set<String> failedPNRs = new HashSet<>();
        if (nonSplitNAgentTnxMissingPNRs.size() > 0) {
            ReservationLot lot = new ReservationLot(tuple, nonSplitNAgentTnxMissingPNRs, fromDate, toDate);
            failedPNRs =  fixMissingAgentTransactionForLot(lot, tuple, updateFailedPNRs);
        }

        return failedPNRs;
    }

    private Set<String> fixAllSplitRelatedPNRs(String fromDate, String toDate, Map<String, ParentPNRDTO> splitConsideredPNRMaps,
                                               Map<String, Set<SplitPNRDTO>> fullMappedPNRs, CarrierTuple tuple,
                                               Set<CarrierTuple> tuples, Set<String> failedPNRs) {

        List<SplitReservationLot> lots = splitConsideredPNRMaps.keySet().stream().map(parentPNR ->
                new SplitReservationLot(tuple, splitConsideredPNRMaps.get(parentPNR),
                        fullMappedPNRs.get(parentPNR))).collect(Collectors.toList());
        Set<String> updateFailedPNRs = new HashSet<>();
        for (SplitReservationLot lot : lots) {
            Set<String> failedPnrsRes = fixSplitReservationLot(lot, fromDate, toDate, failedPNRs);
            if(failedPnrsRes != null && failedPnrsRes.size()> 0){
                updateFailedPNRs.addAll(failedPnrsRes);
            }
        }


        return updateFailedPNRs;
    }

    private Set<String> fixSplitReservationLot(SplitReservationLot lot, String fromDate, String toDate, Set<String> failedPNRs) {
        Set<String> parentPNRSet = new HashSet<>();
        parentPNRSet.add(lot.parentPNRDTO.getPnr());
        Map<String, BasicReservationDTO> mktPnrDetails = reservationJdbcRepo.getBasicReservationDetailsForPnrs(parentPNRSet
                , lot.tuple);
        final ExchangeRateProcessor exProcessor = loadExchangeRates(lot.tuple.mktCarrier,
                lot.tuple.mktCarrier, fromDate, toDate);

        Map<String, List<PaxExtTransactionDTO>> mktPaxExtTnxDetails = reservationJdbcRepo.getPaxExtTransactionsForPnrs(parentPNRSet, new CarrierTuple(lot.tuple.mktCarrier, null), null, null);
        Map<String, List<PaxTransactionDTO>> mktPaxTnxDetails = reservationJdbcRepo.getPaxTransactionsForPnrs(parentPNRSet, lot.tuple, null, null);
        Map<String, List<AgentTransactionDTO>> mktAgentTnxDetails = reservationJdbcRepo.getAgentTransactionForPnrs(parentPNRSet, lot.tuple, null, null);
        Map<String, List<PassengerDTO>> mktPaxDetails = reservationJdbcRepo.getPassengerDetailsForPnrs(parentPNRSet, lot.tuple);


        mktPaxExtTnxDetails = nullSafeMap(mktPaxExtTnxDetails);
        mktPaxTnxDetails = nullSafeMap(mktPaxTnxDetails);
        mktAgentTnxDetails = nullSafeMap(mktAgentTnxDetails);
        mktPaxDetails = nullSafeMap(mktPaxDetails);
        logger.debug("FIX SPLIT RESERVATION: Parent PNR:{}", lot.parentPNRDTO.getPnr());
        logger.debug("FIX SPLIT RESERVATION: Child PNRs:{}", lot.splitPNRDTOS.stream().map(SplitPNRDTO::getChildPNR).collect(Collectors.toSet()));

        for (SplitPNRDTO splitDTO : lot.splitPNRDTOS) {
            Set<String> childPNRSet = new HashSet<>();
            childPNRSet.add(splitDTO.getChildPNR());
            String splitToDate = CommonUtil.formatDate(splitDTO.getSplitDate());
            Map<String, List<PaxExtTransactionDTO>> paxExtTnxs = reservationJdbcRepo.getPaxExtTransactionsForPnrs(childPNRSet, new CarrierTuple(lot.tuple.mktCarrier, null), fromDate, splitToDate);
            Map<String, List<PaxTransactionDTO>> paxTxns = reservationJdbcRepo.getPaxTransactionsForPnrs(childPNRSet, lot.tuple, fromDate, splitToDate);
            Map<String, List<AgentTransactionDTO>> agtTnx = reservationJdbcRepo.getAgentTransactionForPnrs(childPNRSet, lot.tuple, fromDate, splitToDate);
            Map<String, List<PassengerDTO>> paxDetails = reservationJdbcRepo.getPassengerDetailsForPnrs(childPNRSet, lot.tuple);

            Map<String, List<PassengerDTO>> parentPaxDetails = new HashMap<>();
            parentPaxDetails.put(lot.parentPNRDTO.getPnr(), paxDetails.values().stream()
                    .flatMap(Collection::stream).collect(Collectors.toList()));

            paxExtTnxs = nullSafeMap(paxExtTnxs);
            paxTxns = nullSafeMap(paxTxns);
            agtTnx = nullSafeMap(agtTnx);
            parentPaxDetails = nullSafeMap(parentPaxDetails);

            mktPaxExtTnxDetails = mergeMaps(mktPaxExtTnxDetails, paxExtTnxs);
            mktPaxTnxDetails = mergeMaps(mktPaxTnxDetails, paxTxns);
            mktAgentTnxDetails = mergeMaps(mktAgentTnxDetails, agtTnx);
            mktPaxDetails = mergeMaps(mktPaxDetails, parentPaxDetails);
        }
        List<PaxExtTransactionDTO> paxExtTns = mktPaxExtTnxDetails.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
        Map<String, List<PaxTransactionDTO>> allPaxTnxMap = new HashMap<>(mktPaxTnxDetails);

        List<ReservationDTO> reservations = getAgentTransactionCorrectedReservations(mktAgentTnxDetails, mktPnrDetails, mktPaxDetails, paxExtTns, allPaxTnxMap, lot.tuple.mktCarrier, exProcessor);
        return correctAllReservations(reservations, failedPNRs);
    }

    private <K, T> Map<K, List<T>> mergeMaps(Map<K, List<T>> map1, Map<K, List<T>> map2) {
        return Stream.of(map1, map2).flatMap(map -> map.entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (v1, v2) -> {
                    List<T> list = new ArrayList<>();
                    list.addAll(v1);
                    list.addAll(v2);
                    return list;
                }));
    }

    private <K, T> Map<K, T> nullSafeMap(Map<K, T> map) {
        return (map == null) ? new HashMap<>() : map;
    }

    private Map<String, Set<SplitPNRDTO>> getAllSplitRecursiveSplits(String fromDate, String toDate, Set<CarrierTuple> tuplesdup, Map<String, ParentPNRDTO> splitConsideredPNRMaps) {
        Map<String, Set<SplitPNRDTO>> fullMappedPNRs = new HashMap<>();
        Set<String> nextSet = splitConsideredPNRMaps.keySet();
        Map<String, String> childToParentMap = new HashMap<>();
        int j = 0;
        Set<String> traversedSet = new HashSet<>();
        Set<CarrierTuple> alltuples = Stream.of(CommonUtil.ALL_CARRIER_CODES)
                .map(code -> new CarrierTuple(code, code)).collect(Collectors.toSet());
        while (nextSet != null && nextSet.size() > 0) {
            Set<String> nextRound = new HashSet<>();
            logger.debug("PNRs for subpnr detection count:{}, pnrs: {}", nextSet.size(), nextSet);
            for (CarrierTuple carrierTuple : alltuples) {
                Map<String, Set<SplitPNRDTO>> newPNRsMap = reservationJdbcRepo.getNewPNRsForSplitted(nextSet,
                        fromDate, toDate, carrierTuple);
                traversedSet.addAll(nextSet);

                if (newPNRsMap.size() > 0) {
                    newPNRsMap.values().stream().flatMap(Collection::stream).forEach(
                            splitDto -> childToParentMap.put(splitDto.getChildPNR(), splitDto.getParentPNR())
                    );

                    nextRound.addAll(newPNRsMap.values().stream().flatMap(Collection::stream)
                            .map(SplitPNRDTO::getChildPNR).collect(Collectors.toSet()));

                    fullMappedPNRs = Stream.of(fullMappedPNRs, newPNRsMap)
                            .flatMap(map -> map.entrySet().stream()).collect(Collectors.toMap(k -> findSuperParentKey(childToParentMap, k.getKey()), Map.Entry::getValue, (v1, v2) -> {
                                Set<SplitPNRDTO> set = new HashSet<>(v1);
                                set.addAll(v2);
                                return set;
                            }));
                }

            }
            nextRound.removeAll(traversedSet);
            nextSet = nextRound;


            if (j > 50) {
                logger.warn("===>BREAK REACHED<== EXITING LOOP, nextSet:{}", nextSet);
                throw new RuntimeException("EXITING PROGRAM AS LOOPING IS CRAZY");
            }
            j++;
        }
        return fullMappedPNRs;
    }

    private Map<String, ParentPNRDTO> getSplitPNRsFromAll(String fromDate, String toDate, Set<String> allPNRs, Set<CarrierTuple> tuplesdup) {
        Map<String, ParentPNRDTO> splitConsideredPNRMaps = new HashMap<>();
        Map<String, Long> counters = new HashMap<>();
        for (CarrierTuple carrierTuple : tuplesdup) {
            Map<String, ParentPNRDTO> splitPNRs = reservationJdbcRepo.getSplittedPNRs(allPNRs, fromDate, toDate, carrierTuple);
            if (splitPNRs != null) {
                splitConsideredPNRMaps = Stream.of(splitConsideredPNRMaps, splitPNRs).flatMap(map -> map.entrySet().stream()).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (v1, v2) -> {
                    v1.addCarrierCodes(v2.getCarrierCodes());
                    v1.addCarrierCodes(carrierTuple.opCarrier);
                    return v1;
                }));
            }
        }

        for (String pnr : splitConsideredPNRMaps.keySet()) {
            String tostr = splitConsideredPNRMaps.get(pnr).getCarrierCodes().stream().sorted().collect(Collectors.toList()).toString();
            if (!counters.containsKey(tostr)) {
                counters.put(tostr, 0L);
            }
            counters.put(tostr, counters.get(tostr) + 1L);
        }
        counters.keySet().forEach(c -> logger.debug("Split carrier comb : {}, pnrcount: {}", c, counters.get(c)));
        logger.debug(" split  pnrs count:{} pnrs: {}", splitConsideredPNRMaps.keySet().size(),
                splitConsideredPNRMaps.keySet());
        return splitConsideredPNRMaps;
    }

    private String findSuperParentKey(Map<String, String> finalFullMappedPNRs, String key) {
        if (finalFullMappedPNRs.containsKey(key)) {
            findSuperParentKey(finalFullMappedPNRs, finalFullMappedPNRs.get(key));
        }
        return key;
    }

    private Set<String> fixAgentTransactionMissingOnly(String fromDate, String toDate, CarrierTuple tuple,
                                                       List<AgentTnxPaxTnxDTO> agentTnxPaxTnxMismatches,
                                                       Set<String> allSplitRelatedPNRs, Set<String> updateFailedPNRs) {
        Set<String> tnxMissingPnrs = agentTnxPaxTnxMismatches.stream().
                filter(item -> item.getAtPNR() == null && item.getPtPNR() != null &&
                        !allSplitRelatedPNRs.contains(item.getPtPNR()))
                .map(AgentTnxPaxTnxDTO::getPtPNR).collect(Collectors.toSet());
        logger.debug("AGT tnx missing pnrs count:{} pnrs: {}", tnxMissingPnrs.size(), tnxMissingPnrs);
        Set<String> failedPNRsList = new HashSet<>();
        if (tnxMissingPnrs.size() > 0) {
            ReservationLot lot = new ReservationLot(tuple, tnxMissingPnrs, fromDate, toDate);
            failedPNRsList = fixMissingAgentTransactionForLot(lot, tuple, updateFailedPNRs);
        }
        return failedPNRsList;
    }

    private Set<String> fixMissingAgentTransactionForLot(ReservationLot lot, CarrierTuple tuple, Set<String> failedPNRs) {
        Map<String, List<PaxExtTransactionDTO>> mktPaxExtTnxDetails = reservationJdbcRepo.getPaxExtTransactionsForPnrs(lot.pnrs, new CarrierTuple(tuple.mktCarrier, null), null, null);
        Map<String, List<PaxTransactionDTO>> mktPaxTnxDetails = reservationJdbcRepo.getPaxTransactionsForPnrs(lot.pnrs, tuple, null, null);
        Map<String, List<AgentTransactionDTO>> mktAgentTnxDetails = reservationJdbcRepo.getAgentTransactionForPnrs(lot.pnrs, tuple, null, null);
        Map<String, BasicReservationDTO> mktPnrDetails = reservationJdbcRepo.getBasicReservationDetailsForPnrs(lot.pnrs, lot.carrierTuple);
        Map<String, List<PassengerDTO>> mktPaxDetails = reservationJdbcRepo.getPassengerDetailsForPnrs(lot.pnrs, lot.carrierTuple);

        List<PaxExtTransactionDTO> paxExtTns = mktPaxExtTnxDetails.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
        Map<String, List<PaxTransactionDTO>> allPaxTnxMap = new HashMap<>(mktPaxTnxDetails);

        List<ReservationDTO> reservations = getAgentTransactionCorrectedReservations(mktAgentTnxDetails, mktPnrDetails, mktPaxDetails, paxExtTns, allPaxTnxMap, lot.carrierTuple.mktCarrier, null);

        return correctAllReservations(reservations, failedPNRs);

    }

    private List<ReservationDTO> getAgentTransactionCorrectedReservations(Map<String, List<AgentTransactionDTO>> mktAgentTnxDetails,
                                                                          Map<String, BasicReservationDTO> mktPnrDetails,
                                                                          Map<String, List<PassengerDTO>> mktPaxDetails,
                                                                          List<PaxExtTransactionDTO> paxExtTns,
                                                                          Map<String, List<PaxTransactionDTO>> allPaxTnxMap, String carrierCode, ExchangeRateProcessor exProcessor) {
        for (PaxExtTransactionDTO extTnx : paxExtTns) {
            PaxTransactionDTO tnx = convertExtToInternal(extTnx);
            if (!allPaxTnxMap.containsKey(tnx.getUniquePaxSeqNo())) {
                allPaxTnxMap.put(tnx.getUniquePaxSeqNo(), new ArrayList<>());
            }
            allPaxTnxMap.get(tnx.getUniquePaxSeqNo()).add(tnx);
        }
        List<ReservationDTO> reservations = new ArrayList<>();
        for (String pnr : mktPnrDetails.keySet()) {
            ReservationDTO res = new ReservationDTO();
            AgentKeyUtil keyUtil = new AgentKeyUtil();
            res.setBasic(mktPnrDetails.get(pnr));
            res.setPassengers(mktPaxDetails.get(pnr));
            for (PassengerDTO pax : mktPaxDetails.get(pnr)) {
                if (allPaxTnxMap.containsKey(pax.getUniquePaxSeqNo())) {
                    pax.getPaxTransactions().addAll(allPaxTnxMap.get(pax.getUniquePaxSeqNo()));
                }
            }
            convertCurrencyValues(res, exProcessor, carrierCode);
            List<AgentTransactionDTO> agentTransactions = deriveAgentTransactions(res, false);
            Map<String, AgentTransactionDTO> mktAgtTnxMap = new HashMap<>();

            if (mktAgentTnxDetails.containsKey(pnr)) {
                for (AgentTransactionDTO agentTransactionDTO : mktAgentTnxDetails.get(pnr)) {
                    String agentKey = keyUtil.getAgentTnxKey(agentTransactionDTO.getDataMap());
                    if (!mktAgtTnxMap.containsKey(agentKey)) {
                        mktAgtTnxMap.put(agentKey, agentTransactionDTO);
                    } else {
                        AgentTransactionDTO agtTnx = mktAgtTnxMap.get(agentKey);
                        addValueToFirst(agtTnx.getDataMap().get("AMOUNT"), agentTransactionDTO.getDataMap().get("AMOUNT"));
                        addValueToFirst(agtTnx.getDataMap().get("AMOUNT_LOCAL"),
                                agentTransactionDTO.getDataMap().get("AMOUNT_LOCAL"));
                    }
                }
            }
            try {
                agentTransactions = markAgentTnxForCorrection(pnr, keyUtil, agentTransactions, mktAgtTnxMap);
                res.setAgentTransactions(agentTransactions);
                reservations.add(res);
            }catch (InvalidDataException ide){
                logger.error("PNR skipped due to invalid data: {}",res.getBasic().getPNR(),ide);
            }
        }
        return reservations;
    }

    private Set<String> correctAllReservations(List<ReservationDTO> reservations, Set<String> failedPNRs) {
        logger.debug("==== CORRECT RESERVATIONS =====");
        Set<String> failedPNRsRes = new HashSet<>();
        for (ReservationDTO res : reservations) {
            try {
                if(failedPNRs!= null && failedPNRsRes.contains(res.getBasic().getPNR())){
                    logger.warn("=== WARNING : PNR is skipped due to previous update errors. PNR: {}",res.getBasic().getPNR());
                } else {
                    correctReservationPerPNR(res);
                }
            } catch (Exception e) {
                failedPNRsRes.add(res.getBasic().getPNR());
                logger.error("UPDATE Failed for pnr:{}", res.getBasic().getPNR(), e);
            }
        }
        return failedPNRsRes;
    }

    private List<AgentTransactionDTO> markAgentTnxForCorrection(String pnr, AgentKeyUtil keyUtil, List<AgentTransactionDTO> agentTransactions, Map<String, AgentTransactionDTO> mktAgtTnxMap) {
        logger.debug("===== AGENT TRNX MISMATCH DETECTION AND COMPENSATION ===== ");
        Set<String> derivedAgentTnxKeys = agentTransactions.stream().map(t -> keyUtil.getAgentTnxKey(t.getDataMap())).collect(Collectors.toSet());
        Set<String> agentTnxKeys = new HashSet<>(mktAgtTnxMap.keySet());

        logger.warn("Agent transaction sizes pnr:{} paxderiverkeys:{}, agenttnxkeys:{}",pnr, derivedAgentTnxKeys.size(), agentTnxKeys.size());

        BigDecimal paxAgtTotal = agentTransactions.stream().map(AgentTransactionDTO::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal agtTotal = mktAgtTnxMap.values().stream().map(AgentTransactionDTO::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);

        if (paxAgtTotal.compareTo(agtTotal) != 0) {
            logger.warn("Pax agent and agent totals are not matching pnr{} pax:{} agt:{} diff:{}",pnr, paxAgtTotal,
                    agtTotal, paxAgtTotal.subtract(agtTotal).abs());
            logger.warn("pnr:{} Derived tnx: {} agent tnx:{}", pnr,derivedAgentTnxKeys.size(), agentTnxKeys.size());
            logger.warn("pnr:{} Derived tnx keys: {} agent tnx keys:{}",pnr, derivedAgentTnxKeys, agentTnxKeys);

            if (paxAgtTotal.subtract(agtTotal).abs().compareTo(new BigDecimal("1")) >= 0) {
                logger.warn("DIFFERENCE IS HIGH: {}",paxAgtTotal.subtract(agtTotal).abs());
            }
        }
        List<AgentTransactionDTO> tnxToBeAdded = new ArrayList<>();
        BigDecimal totalDiff = paxAgtTotal.subtract(agtTotal).abs();
        logger.debug("pnr:{} Total difference : {}",pnr, totalDiff);
        List<AgentTransactionDTO> clonedTnxs = new ArrayList<>();
        if (totalDiff.compareTo(BigDecimal.ZERO) != 0 && totalDiff.compareTo(new BigDecimal("0.02")) <= 0
                || derivedAgentTnxKeys.size() != agentTnxKeys.size()) {
            agentTransactions.forEach(tnxItem -> {
                AgentTransactionDTO tnx = tnxItem.clone();
                clonedTnxs.add(tnx);
                String key = keyUtil.getAgentTnxKey(tnx.getDataMap());
                AgentTransactionDTO mktTnx = mktAgtTnxMap.get(key);

                if (mktTnx != null && mktTnx.getAmount().compareTo(tnx.getAmount()) == 0) {
                    tnx.setNeedSync(false);
                    logger.info("Tnx matching key {} , set: {}", key, mktAgtTnxMap.keySet());
                } else if (mktTnx != null) {
                    BigDecimal diff = mktTnx.getAmount().subtract(tnx.getAmount()).abs();
                    if ((diff.compareTo(new BigDecimal("0.02")) <= 0)) {
                        tnx.setNeedSync(false);
                        logger.warn("SMALL MISMATCH: Decimal mismatch agent:{} pnr:{} key:{} diff:{}", tnx.getAgentCode(), tnx.getPNR(), key, diff);
                    } else {
                        tnx.setNeedSync(false);
                        logger.warn("NEED MANUAL ACTION: Tnx partially recorded agent:{} pnr:{} key:{} diff:{}", tnx.getAgentCode(), tnx.getPNR(), key, diff);
                        logger.warn("NEED MANUAL ACTION: Tnx partially recorded derived:{}, agt:{}", tnx.getAmount().toPlainString(), mktTnx.getAmount().toPlainString());

                        BigDecimal diffAmt = mktTnx.getAmount().subtract(tnx.getAmount());
                        AgentTransactionDTO tnxClone = tnx.clone();
                        if (diffAmt.compareTo(BigDecimal.ZERO) > 0) {
                            //REFUND MONEY TO AGENT
                            tnxClone.setNominalCodesFor("CR");
                        } else {
                            tnxClone.setNominalCodesFor("DR");
                            //CHARGE AGENT
                        }
                        tnxClone.setNeedSync(true);
                        tnxClone.setAmount(diffAmt);
                        tnxClone.setAmountLocal(mktTnx.getAmountLocal().subtract(tnx.getAmountLocal()));
                        tnxToBeAdded.add(tnxClone);
                    }
                } else {
                    tnx.setNeedSync(true);
                    logger.info("Tnx missing pnr:{}, key {} , set: {}",pnr, key, mktAgtTnxMap.keySet());
                }
            });
            BigDecimal totalCorrectedAmt = tnxToBeAdded.stream().map(AgentTransactionDTO::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add).abs();
            if (totalCorrectedAmt.subtract(totalDiff).abs().compareTo(new BigDecimal("0.02")) <= 0 && tnxToBeAdded.size() > 0) {
                logger.warn("pnr:{}, New tnxs to be added :{}", pnr, tnxToBeAdded.size());
                clonedTnxs.addAll(tnxToBeAdded);
            } else if (totalCorrectedAmt.subtract(totalDiff).abs().compareTo(new BigDecimal("0.02")) > 0) {
                logger.warn("WRONG CORRECTION SKIPPED :: pnr {}, Corrected amt {},  diff between total and corection:{}", pnr,
                        totalCorrectedAmt, totalCorrectedAmt.subtract(totalDiff));

            }

            derivedAgentTnxKeys = clonedTnxs.stream().map(t -> keyUtil.getAgentTnxKey(t.getDataMap())).collect(Collectors.toSet());
            if (derivedAgentTnxKeys.size() < agentTnxKeys.size() - tnxToBeAdded.size()) {
                logger.error("Problem with the derived tnx and agent trnx correction derivedTnxKeys:{}, agentTnxKeys: {}, pnr:{} ",
                        derivedAgentTnxKeys.size(), agentTnxKeys.size(), pnr);
                throw new InvalidDataException("Problem with the derived tnx and agent trnx correction  "
                );
            } else if (derivedAgentTnxKeys.size() != agentTnxKeys.size()) {
                logger.warn("Problem with the derived tnx and agent trnx correction derivedTnxKeys:{}, agentTnxKeys: {}, pnr:{} ",
                        derivedAgentTnxKeys.size(), agentTnxKeys.size(), pnr);
            }
        }


        return clonedTnxs;
    }

    private PaxTransactionDTO convertExtToInternal(PaxExtTransactionDTO extTnx) {
        PaxTransactionDTO tnx = new PaxTransactionDTO(extTnx.getCarrierCode());
        Map<String, String> extToIntMap = tblConfig.getTableToTableMapping("t_pax_ext_carrier_tnx_to_t_pax_tnx");
        for (String extKey : extToIntMap.keySet()) {
            String intKey = extToIntMap.get(extKey);
            tnx.addData(intKey, extTnx.getDataMap().get(extKey).clone());
        }
        return tnx;
    }

    private List<ReservationLot> getReservationMissingExtTransactions(List<CarrierTuple> tuples, String fromDate, String toDate) {
        List<ReservationLot> correctionLots = new ArrayList<>();
        tuples.forEach(tuple -> {
            logger.info("Locating missing tnrxs for mkt: {}, op : {}", tuple.mktCarrier, tuple.opCarrier);

            Map<String, Set<String>> pnrTnxIds = reservationJdbcRepo.getMissingExtTransactions(fromDate,
                    toDate, tuple);
            logger.debug("for mkt: {}, op : {} # of pnrs: {}, pnrs : {}", tuple.mktCarrier, tuple.opCarrier,
                    pnrTnxIds.keySet().size(), pnrTnxIds.keySet());
            if (pnrTnxIds.keySet().size() > 0) {
                correctionLots.add(new ReservationLot(tuple, pnrTnxIds, fromDate, toDate));
            }
        });
        return correctionLots;
    }

    private Set<String> correctReservationProblemPnrsLots(List<ReservationLot> correctionLots, Set<String> updateFailedPNRs) {
        Set<String> resFailedPNRs = new HashSet<>();
        correctionLots.forEach(lot -> {
            if (lot.pnrs != null && lot.pnrs.size() > 0) {
                Set<String> failedPNRs = correctReservationPNRsLot(lot, updateFailedPNRs);
                if(failedPNRs!=null && failedPNRs.size() >0){
                    resFailedPNRs.addAll(failedPNRs);
                }
            }
        });
        return resFailedPNRs;
    }

    private Set<String> correctReservationPNRsLot(ReservationLot lot, Set<String> updateFailedPNRs) {
        final ExchangeRateProcessor exProcessor = loadExchangeRates(lot.carrierTuple.mktCarrier,
                lot.carrierTuple.opCarrier, lot.fromDate, lot.toDate);
        Map<String, BasicReservationDTO> opPnrDetails = reservationJdbcRepo.getBasicReservationDetailsForPnrs(lot.pnrs, lot.carrierTuple);
        Map<String, List<PassengerDTO>> opPaxDetails = reservationJdbcRepo.getPassengerDetailsForPnrs(lot.pnrs, lot.carrierTuple);
        Map<String, List<PaxTransactionDTO>> opPaxTnxDetails = reservationJdbcRepo.getPaxTransactionsForPnrs(lot.pnrs, lot.carrierTuple, null, null);
        CarrierTuple mktOnlyTuple = new CarrierTuple(lot.carrierTuple.mktCarrier, lot.carrierTuple.mktCarrier);
        Map<String, BasicReservationDTO> mktPnrDetails = reservationJdbcRepo.getBasicReservationDetailsForPnrs(lot.pnrs, mktOnlyTuple);
        Map<String, List<PassengerDTO>> mktPaxDetails = reservationJdbcRepo.getPassengerDetailsForPnrs(lot.pnrs, mktOnlyTuple);
        Map<String, List<PaxExtTransactionDTO>> mktPaxExtTnxDetails = reservationJdbcRepo.getPaxExtTransactionsForPnrs(lot.pnrs, lot.carrierTuple, null, null);
        Map<String, List<AgentTransactionDTO>> mktAgentTnxDetails = reservationJdbcRepo.getAgentTransactionForPnrs(lot.pnrs, mktOnlyTuple, null, null);

        Set<String> pnrs = new HashSet<>(opPnrDetails.keySet());
        pnrs.removeAll(mktPnrDetails.keySet());
        for (String pnr : pnrs) {
            opPnrDetails.get(pnr).setNeedSync(true);
        }
        pnrs = new HashSet<>(opPnrDetails.keySet());
        for (String pnr : pnrs) {
            Set<String> mktPaxSeq = new HashSet<>();
            if (mktPaxDetails.containsKey(pnr))
                mktPaxSeq = mktPaxDetails.get(pnr).stream().map(PassengerDTO::getUniquePaxSeqNo).collect(Collectors.toSet());
            Set<String> finalMktPaxSeq = mktPaxSeq;
            opPaxDetails.get(pnr).stream().filter(pax -> !finalMktPaxSeq.contains(pax.getUniquePaxSeqNo())).forEach(pax -> pax.setNeedSync(true));
        }
        Set<String> mkLccTnxIds = mktPaxExtTnxDetails.values().stream().flatMap(Collection::stream)
                .map(PaxExtTransactionDTO::getLCCTnxId).collect(Collectors.toSet());
        opPaxTnxDetails.values().stream().flatMap(Collection::stream)
                .filter(paxTnx -> !mkLccTnxIds.contains(paxTnx.getLCCTnxId())).forEach(tnx -> tnx.setNeedSync(true));

        List<ReservationDTO> reservations = new ArrayList<>();
        for (String pnr : opPnrDetails.keySet()) {
            AgentKeyUtil ketUtil = new AgentKeyUtil();
            logger.debug("==== PNR LOT Correction Attempt for PNR: {}", pnr);
            ReservationDTO res = new ReservationDTO();
            res.setBasic(opPnrDetails.get(pnr));
            res.setPassengers(opPaxDetails.get(pnr));
            for (PassengerDTO pax : opPaxDetails.get(pnr)) {
                if (opPaxTnxDetails.containsKey(pax.getUniquePaxSeqNo())) {
                    pax.getPaxTransactions().addAll(opPaxTnxDetails.get(pax.getUniquePaxSeqNo()));
                } else {
                    logger.warn("Missing op tnxs: {} {}", pax.getUniquePaxSeqNo(), opPaxTnxDetails);
                }
            }
            convertCurrencyValues(res, exProcessor, mktOnlyTuple.mktCarrier);
            List<AgentTransactionDTO> agentTransactions = deriveAgentTransactions(res, false);

            Map<String, AgentTransactionDTO> mktAgtTnxMap = new HashMap<>();
            if (mktAgentTnxDetails.get(pnr) != null) {
                for (AgentTransactionDTO agentTransactionDTO : mktAgentTnxDetails.get(pnr)) {
                    mktAgtTnxMap.put(ketUtil.getAgentTnxKey(agentTransactionDTO.getDataMap()), agentTransactionDTO);
                    mktAgtTnxMap.put(ketUtil.getAgentTnxKey(agentTransactionDTO.getDataMap()), agentTransactionDTO);

                }
            }
            agentTransactions = markAgentTnxForCorrection(pnr, ketUtil, agentTransactions, mktAgtTnxMap);
            res.setAgentTransactions(agentTransactions);
            reservations.add(res);
        }

        return correctAllReservations(reservations, updateFailedPNRs);
    }


    private List<AgentTransactionDTO> deriveAgentTransactions(ReservationDTO res, boolean considerSync) {
        String[] commonFields = new String[]{"AERO_AGENT_CREDIT_REF_ID",
                "AERO_PAYMENT_REF_ID", "AEROMART_REF_ID", "AGENT_CODE", "AMOUNT", "DR_CR",
                "EXT_REFERENCE", "NOMINAL_CODE", "SALES_CHANNEL_CODE", "TNX_DATE", "USER_ID", "VERSION"};
        Map<String, String> mappedFields = new HashMap<>();
        mappedFields.put("AMOUNT_LOCAL", "AMOUNT_PAYCUR");
        mappedFields.put("CURRENCY_CODE", "PAYMENT_CURRENCY_CODE");
        Map<String, AgentTransactionDTO> agentTnxs = new HashMap<>();
        AgentKeyUtil keyUtil = new AgentKeyUtil();
        for (PassengerDTO pax : res.getPassengers()) {
            for (PaxTransactionDTO paxTrnx : pax.getPaxTransactions()) {
                if (considerSync && !paxTrnx.isNeedSync()) {
                    continue;
                }
                String key = keyUtil.getAgentTnxKey(paxTrnx.getDataMap());
                if (!agentTnxs.containsKey(key)) {
                    AgentTransactionDTO agentTnx = new AgentTransactionDTO(paxTrnx.getCarrierCode());
                    for (String field : commonFields) {
                        agentTnx.getDataMap().put(field, paxTrnx.getDataMap().get(field).clone());
                    }
                    for (String field : mappedFields.keySet()) {
                        agentTnx.getDataMap().put(field, paxTrnx.getDataMap().get(mappedFields.get(field)).clone());
                    }
                    agentTnx.getDataMap().put("PNR", res.getBasic().getDataMap().get("PNR").clone());
                    agentTnx.getDataMap().put("AMOUNT", negateValue(agentTnx.getDataMap().get("AMOUNT").clone()));
                    agentTnx.getDataMap().put("AMOUNT_LOCAL", negateValue(agentTnx.getDataMap().get("AMOUNT_LOCAL").clone()));
                    GenericDataDTO data = agentTnx.getDataMap().get("NOMINAL_CODE");
                    Long paxTnxNominalCode = (Long) data.getValue();
                    if (paxTnxNominalCode == 19) {
                        agentTnx.setNominalCodesFor("DR");
                    } else if (paxTnxNominalCode == 25) {
                        agentTnx.setNominalCodesFor("CR");
                    }
                    agentTnxs.put(key, agentTnx);
                } else {
                    AgentTransactionDTO agentTnx = agentTnxs.get(key);
                    addValueToFirst(agentTnx.getDataMap().get("AMOUNT"), negateValue(paxTrnx.getDataMap().get("AMOUNT")));
                    addValueToFirst(agentTnx.getDataMap().get("AMOUNT_LOCAL"),
                            negateValue(paxTrnx.getDataMap().get("AMOUNT_PAYCUR")));

                }
            }
        }
        return new ArrayList<>(agentTnxs.values());
    }

    private void addValueToFirst(GenericDataDTO firstData, GenericDataDTO secondData) {
        BigDecimal firstAmt = (BigDecimal) firstData.getValue();
        BigDecimal secondAmt = (BigDecimal) secondData.getValue();
        firstAmt = firstAmt.add(secondAmt);
        firstData.setValue(firstAmt);
    }

    private GenericDataDTO negateValue(GenericDataDTO data) {
        if (data.isCurrencyValue()) {
            data = data.clone();
            BigDecimal amount = (BigDecimal) data.getValue();
            data.setValue(amount.negate());
        }
        return data;
    }

    private String getAxgentTnxKey(Map<String, GenericDataDTO> dataMap) {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-dd-MM HH:mm");
        sb.append(sdf.format((Date) dataMap.get("TNX_DATE").getValue()));
        sb.append("|");
        sb.append(dataMap.get("AGENT_CODE").getValue().toString());
        sb.append("|");
        sb.append(dataMap.get("NOMINAL_CODE").getValue().toString());
        return sb.toString();
    }

    private void convertCurrencyValues(ReservationDTO res, ExchangeRateProcessor exProcessor, String targetCarrier) {
        if (exProcessor == null || targetCarrier == null) {
            return;
        }
        List<GenericDataDTO> preservedRes = new ArrayList<>();
        for (GenericDataDTO data : res.getBasic().getDataMap().values()) {
            if (data.needToConvert()) {
                if (data.isPreserveOriginal()) {
                    GenericDataDTO clone = data.clone();
                    preservedRes.add(clone);
                }
                BigDecimal value = (BigDecimal) data.getValue();
                BigDecimal convertedValue = exProcessor.convert(res.getBasic().getBookingDate(), value,
                        res.getBasic().getCarrierCode(), targetCarrier);
                data.setValue(convertedValue);
            }
        }
        for (GenericDataDTO data : preservedRes) {
            res.getBasic().addData("ORI_" + data.getKey(), data);
        }


        for (PassengerDTO pax : res.getPassengers()) {
            List<GenericDataDTO> preservedPax = new ArrayList<>();

            for (GenericDataDTO data : pax.getDataMap().values()) {
                if (data.needToConvert()) {
                    if (data.isPreserveOriginal()) {
                        GenericDataDTO clone = data.clone();
                        preservedPax.add(clone);
                    }
                    BigDecimal value = (BigDecimal) data.getValue();
                    BigDecimal convertedValue = exProcessor.convert(res.getBasic().getBookingDate(), value,
                            pax.getCarrierCode(), targetCarrier);
                    data.setValue(convertedValue);
                }
            }
            for (GenericDataDTO data : preservedPax) {
                pax.addData("ORI_" + data.getKey(), data);
            }
            for (PaxTransactionDTO tnx : pax.getPaxTransactions()) {
                List<GenericDataDTO> preservedPaxTnxs = new ArrayList<>();

                for (GenericDataDTO data : tnx.getDataMap().values()) {
                    if (data.needToConvert()) {
                        if (data.isPreserveOriginal()) {
                            GenericDataDTO clone = data.clone();
                            preservedPaxTnxs.add(clone);
                        }
                        BigDecimal value = (BigDecimal) data.getValue();
                        BigDecimal convertedValue = exProcessor.convert(tnx.getTransactionDate(), value,
                                tnx.getCarrierCode(), targetCarrier);
                        data.setValue(convertedValue);
                    }
                }
                for (GenericDataDTO data : preservedPaxTnxs) {
                    tnx.addData("ORI_" + data.getKey(), data);
                }
            }
        }


    }


    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {SQLException.class, DataAccessException.class})
    private void correctReservationPerPNR(ReservationDTO res) {
        logger.debug("==== CORRECT RESERVATION BY RESERVATION =====");
        String pnr = res.getBasic().getPNR();
        if (res.getBasic().isNeedSync()) {
            logger.debug("==>SYNC DUMMY CARRIER FOR pnr:{}", pnr);
            reservationJdbcRepo.createMissingDummyReservation(res.getBasic());
        }
        Map<Long, Long> opMktPaxIdMap = new HashMap<>();
        AgentKeyUtil keyUtil = new AgentKeyUtil();
        for (PassengerDTO pax : res.getPassengers()) {
            Long paxIdOp = pax.getPnrPaxId();
            if (pax.isNeedSync()) {
                logger.debug("==>SYNC DUMMY CARRIER FOR paxSeq:{}", pnr, pax.getUniquePaxSeqNo());
                reservationJdbcRepo.createMissingDummyPassenger(pax, res.getBasic().getCarrierTuple());
            }
            if (!properties.isSkipInserts()) {
                PassengerDTO savedDummyPax = reservationJdbcRepo.getPassenger(pax.getPNR(), pax.getPaxSequence(),
                        res.getBasic().getCarrierTuple());
                opMktPaxIdMap.put(paxIdOp, savedDummyPax.getPnrPaxId());
            }
            for (PaxTransactionDTO paxTnx : pax.getPaxTransactions()) {
                if (paxTnx.isNeedSync()) {
                    logger.debug("==>SYNC MISSING EXT PAX TNX FOR paxSeq:{} lccTnxId:{}", pax.getUniquePaxSeqNo(), paxTnx.getLCCTnxId());
                    reservationJdbcRepo.addMissingPaxExtTransactions(paxTnx, opMktPaxIdMap, res.getBasic().getCarrierTuple(),
                            res.getBasic().getPNR(), pax.getPaxSequence());
                }
            }
        }
        for (AgentTransactionDTO agentTnx : res.getAgentTransactions()) {
            if (agentTnx.isNeedSync()) {
                logger.debug("==>SYNC AGT TNX FOR pnr:{} key:{} amt:{}", pnr, keyUtil.getAgentTnxKey(agentTnx.getDataMap()),
                        agentTnx.getAmount().toPlainString());
                reservationJdbcRepo.addMissingAgentTransactions(agentTnx, res.getBasic().getCarrierTuple());
                reservationJdbcRepo.adjustAgentTransactionSummary(agentTnx, res.getBasic().getCarrierTuple());
            }
        }

    }

    private ExchangeRateProcessor loadExchangeRates(String fromCarrier, String toCarrier, String fromDate, String
            toDate) {
        List<ExchangeRateDTO> excRates = null;
        if (!fromCarrier.equals(toCarrier))
            excRates = reservationJdbcRepo.getLCCExchangeRatesFor(fromCarrier,
                    toCarrier, fromDate, toDate);
        if (excRates == null) {
            excRates = new ArrayList<>();
        }
        if (!fromCarrier.equals(toCarrier))
            excRates.addAll(reservationJdbcRepo.getLCCExchangeRatesFor(toCarrier,
                    fromCarrier, fromDate, toDate));

        return new ExchangeRateProcessor(excRates);
    }

    private List<ReservationLot> getDummyReservationMissingPNRs(List<CarrierTuple> tuples, String
            fromDate, String toDate) {
        List<ReservationLot> correctionLots = new ArrayList<>();
        tuples.forEach(tuple -> {
            logger.info("Locating missing pnrs for mkt: {}, op : {}", tuple.mktCarrier, tuple.opCarrier);

            Set<String> pnrs = reservationJdbcRepo.getDummyCarrierMissingPnrs(fromDate,
                    toDate, tuple);
            logger.debug("for mkt: {}, op : {} # of pnrs: {}, pnrs : {}", tuple.mktCarrier, tuple.opCarrier,
                    pnrs.size(), pnrs);
            if (pnrs.size() > 0) {
                correctionLots.add(new ReservationLot(tuple, pnrs, fromDate, toDate));
            }
        });
//        FIXME testing lot
//        ReservationLot lot = new ReservationLot(new CarrierTuple("G9", "3L"),
//                Collections.singleton("11334212"),
//                fromDate, toDate);
//        correctionLots.add(lot);
        return correctionLots;
    }

    private List<CarrierTuple> getCarrierCombinations(Set<String> carriers) {
        List<CarrierTuple> tuples = new ArrayList<>();
        for (String mktCarrier : carriers) {
            for (String opCarrier : carriers) {
                if (!mktCarrier.equals(opCarrier)) {
                    logger.debug("Carrier Combinations:: mktCarrier: {}, opCarrier: {}", mktCarrier, opCarrier);
                    tuples.add(new CarrierTuple(mktCarrier, opCarrier));
                }
            }
        }
        return tuples;
    }
}
