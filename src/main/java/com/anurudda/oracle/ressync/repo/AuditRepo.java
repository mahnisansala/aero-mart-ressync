package com.anurudda.oracle.ressync.repo;

import com.anurudda.oracle.ressync.dto.ConfigProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class AuditRepo {

    private static final Date CURRENT_DATE = new Date();

    Logger logger = LoggerFactory.getLogger(getClass().getName());

    @Autowired
    private ConfigProperties properties ;

    public void auditChanges(String str){
        try {
            String strLC = str.toLowerCase();
            String dateFmt = (new SimpleDateFormat("yyyyMMdd_HHmm").format(CURRENT_DATE));
            String fileName = properties.getLogPath()+"queries"+dateFmt+".txt";
            if(strLC.indexOf("update") >-1 || strLC.indexOf("insert") > -1) {
                fileName = properties.getLogPath()+"updates"+dateFmt+".txt";
            }
            writeTOFile(str, fileName);
            writeTOFile(str, properties.getLogPath()+"all_"+dateFmt+".txt");
        } catch (IOException e) {
           logger.error("Error occurred" , e);
        }
    }

    private void writeTOFile(String str, String fileName) throws IOException {
        BufferedWriter out = new BufferedWriter(
                new FileWriter(fileName, true));

        out.write(str +"\n\n");
        out.close();
    }
}
