package com.anurudda.oracle.ressync;

import com.anurudda.oracle.ressync.dto.ConfigProperties;
import com.anurudda.oracle.ressync.service.ReservationSyncService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.*;

@SpringBootApplication
public class RessyncApplication implements ApplicationRunner {

    Logger logger = LoggerFactory.getLogger(getClass().getName());

    @Autowired
    private ReservationSyncService syncService;
    @Autowired
    private ConfigProperties config;

    public static void main(String[] args) {
        SpringApplication.run(RessyncApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        logger.info("Application started with command-line arguments: {}", Arrays.toString(args.getSourceArgs()));
        logger.info("NonOptionArgs: {}", args.getNonOptionArgs());
        logger.info("OptionNames: {}", args.getOptionNames());

        for (String name : args.getOptionNames()){
            logger.info("arg-" + name + "=" + args.getOptionValues(name));
        }

        doCorrections("01-Feb-2022 00:00:00",
                "15-Feb-2022 23:59:59", "G9", "3L", "E5");
    }

    @Scheduled(cron = "${cron.expression}")
    private void runOnSchedule() {
        Date date = Calendar.getInstance().getTime();
        logger.debug("SCHEDULER :{}", date.toString());
    }

    private void doCorrections(String startDateTime, String endDateTime, String... carrierCodes) {
        Set<String> updateFailedPNRs = new HashSet<>();
        logger.info("Config isSyncDummyBookings:{}",config.isSyncDummyBookings());
        if(config.isSyncDummyBookings()) {
            Set<String> dummyFailedPNRs = syncService.syncMissingDummyBookings(startDateTime,
                    endDateTime, updateFailedPNRs, carrierCodes);
            addToFailedPNRs(updateFailedPNRs, dummyFailedPNRs);
        }
        logger.info("Config isSyncExtTransactions:{}",config.isSyncExtTransactions());
        if(config.isSyncExtTransactions()) {
            Set<String> extTnxFailedPNRs = syncService.syncMissingExtTransactions(startDateTime,
                    endDateTime, updateFailedPNRs, carrierCodes);
            addToFailedPNRs(updateFailedPNRs, extTnxFailedPNRs);
        }
        logger.info("Config isSyncAgentAndPaxTransactions:{}",config.isSyncAgentAndPaxTransactions());
        if(config.isSyncAgentAndPaxTransactions()) {
            Set<String> agtTnxFailedPNRs = syncService.syncAgentAndPaxTransactions(startDateTime,
                    endDateTime, updateFailedPNRs, carrierCodes);
        }
    }

    private void addToFailedPNRs(Set<String> updateFailedPNRs, Set<String> responseFailedPNRs) {
        if (responseFailedPNRs != null && responseFailedPNRs.size() > 0) {
            updateFailedPNRs.addAll(responseFailedPNRs);
        }
    }


}
